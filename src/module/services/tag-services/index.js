import api, { callAxios } from '../common-services/apilist'
import { isObjectEmpty } from '../common-services/common'

let tags = {}

export default class TagServices {
  static async getTags() {
    let url = (api.tags.getAllTags).getUrl()
    if(isObjectEmpty(tags))
      tags = await callAxios({ method: 'get', url })
    return tags.tags
  }

  static async getTagsById(tagId) {
    let tags = await this.getTags()
    tags = tags.filter(tag => tagId===tag.tagId)
    return tags[0]
  }

  static async addTagData(data) {
    await this.getTags()
    let lengthBeforeUpdate = tags.tags.length
    return new Promise((res, rej) => {
      if (tags.tags.unshift(data)>lengthBeforeUpdate) res({ status: 200 })
      rej({status: 401, message: 'Failed to add the tag'})
    })
  }

  static async updateTagData(data) {
    let flag = 0
    tags.tags.forEach((tag, index) => {
      if (tag.tagId === data.tagId) {
        tags[index] = { ...tag, ...data }
        flag = 1
      }
    })
    return new Promise((res, rej) => {
      flag ? res({ status: 200 }) : rej({  message: "tag not found with given id"  })
    })
  }

  static async deleteTagData(tagId) {
    await this.getTags()
    tags.tags = tags.tags.filter(tag => tag.tagId !== tagId)
    return new Promise(res => {
      res({status: 200})
    })
  }
}