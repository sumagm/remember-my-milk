import api, { callAxios } from '../common-services/apilist'
import { isObjectEmpty, stringToDate } from '../common-services/common'

let taskList = {}

const checkForTaskDataValidation = (task, taskData) => {
  return ((taskData.categoryId && taskData.categoryId!==0) && task.taskCategory===taskData.categoryId) || ((taskData.tagId && taskData.tagId!==0 ) && task.taskTags.includes(taskData.tagId))
}

const checkForCategoryTaskDataValidation = (task, taskData) => {
  return (taskData.categoryId && taskData.categoryId!==0) ? task.taskCategory===taskData.categoryId : true
}

const checkForTagTaskDataValidation = (task, taskData) => {
  return (taskData.tagId && taskData.tagId!==0 )? task.taskTags.includes(taskData.tagId) : true
}

export default class TaskServices {
  static async getTaskList() {
    let url = (api.task.getAllTask).getUrl()
    if(isObjectEmpty(taskList))
      taskList = await callAxios({ method: 'get', url })
    return taskList.tasksData
  }

  static async getTaskData(taskId) {
    let taskList = await this.getTaskList()
    taskList = taskList.filter(task => task.taskId === taskId)
    return taskList.length>0?taskList[0]:{}
  }

  static async getCurrentUserTaskData(taskData) {
    let taskList = await this.getTaskList()
    taskList = taskList.filter(task => task.status===taskData.taskStatus)
    return taskList
  }

  static async getSpecifiedCategoryTaskData(taskData) {
    let taskList = await this.getTaskList()
    taskList = taskList.filter(task => checkForCategoryTaskDataValidation(task, taskData) && task.status===taskData.taskStatus)
    return taskList
  }

  static async getSpecifiedTagTaskData(taskData) {
    let taskList = await this.getTaskList()
    taskList = taskList.filter(task => checkForTagTaskDataValidation(task, taskData) && task.status===taskData.taskStatus)
    return taskList
  }

  static async getTaskByDate(from, to, taskData) {
    let allTaskList = await this.getTaskList()
    allTaskList.forEach(row => {
      if(typeof row.addedOn === "string") {
        row.addedOn=stringToDate(row.addedOn);
      }
    })
    return allTaskList.filter((row) => row.addedOn >=to && row.addedOn <=from && row.status === taskData.taskStatus)
  }

  static async addTaskData(data) {
    await this.getTaskList()
    let lengthBeforeUpdate = taskList.tasksData.length
    return new Promise((res, rej) => {
      if (taskList.tasksData.unshift(data)>lengthBeforeUpdate) res(taskList.tasksData)
      rej({status: 401, message: 'Failed to add the task'})
    })
  }

  static async updateTaskData(data,taskIds) {
    await this.getTaskList()
    let flag = 0
    taskList.tasksData.forEach((task, index) => {
      if (taskIds.includes(task.taskId)) {
        taskList.tasksData[index] = { ...task, ...data }
        flag = 1
      }
    })
    return new Promise((res, rej) => {
      flag ? res(taskList.tasksData) : rej({  message: "task not found with given id"  })
    })
  }

  static async removeTagOfTask(taskId,tagId) {
    await this.getTaskList()
    let task = await this.getTaskData(taskId)
    task.taskTags = task.taskTags.filter(tag => tag!==tagId)
    return this.updateTaskData(task,[taskId])
  }

  static async deleteTaskData(taskIds) {
    await this.getTaskList()
    taskList.tasksData = taskList.tasksData.filter(task => !taskIds.includes(task.taskId))
    return new Promise(res => {
      res(taskList.tasksData)
    })
  }
}
