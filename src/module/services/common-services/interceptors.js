import axios from 'axios'
import { ROUTES } from './routes'

axios.defaults.baseURL = 'http://localhost:3000/'

const registerInterceptors = (history) => {
  axios.interceptors.response.use(
    (response) => {
      return response
    },
    (err) => {
      if (!err.response)
        return Promise.reject(new Error('Network Error'))
      let status = err.response.status
      if (status === 401 || status === 403) {
        history.push(ROUTES.BASE)
      }
  
      return Promise.reject(err)
    }
  )
}

export default registerInterceptors