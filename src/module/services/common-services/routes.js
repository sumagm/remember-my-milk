export const ROUTES = {
  ALL_TASKS: "/all-tasks",
  CATEGORY: "/category",
  TAG: "/tag",
  BASE: "/",
  TODAY_TASK: "/today-task",
  TOMORROW_TASK: "/tomorrow-task",
  THIS_WEEK_TASK: "/week-task"
}
