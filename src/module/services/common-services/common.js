import React from 'react'

export const AlertContext = React.createContext({
  alertVisibilityState: false,
  alertMsg: '',
  setAlert: (msg) => {},
  toggleAlert: () => {}
});

export const TIME_IN_MILLISECONDS = {
  oneDay: 86400000, // 1day (1000 * 60 * 60 * 24 * 1)
  twoDays: 172800000, // 2days (1000 * 60 * 60 * 24 * 1)
  oneWeek: 604800000, // 7days (1000 * 60 * 60 * 24 * 7)
  thirtyDays: 2592000000,  // 30days (1000 * 60 * 60 * 24 * 30)
  nextDay: new Date(new Date().getTime() + 24 * 60 * 60 * 1000) //today date + 1
}

export const isObject = (val) => {
  if (val === null) { return false;}
    return ( (typeof val === 'function') || (typeof val === 'object') );
}

export const isObjectEmpty = (obj) => {
  if(obj) return Object.keys(obj).length === 0 && obj.constructor === Object;
  return true;
}

export const stringToDate = (date) => {
  const dateArray = date.split('-');
  const dateArrayInNumber = dateArray.map(el=> parseInt(el))
  return new Date(dateArrayInNumber[0]+'-'+dateArrayInNumber[1]+'-'+dateArrayInNumber[2]).getTime()
}

export const getDateForInputValue = (milliseconds) => {
  if(typeof milliseconds === "number" || milliseconds !== 0) {
    let date = new Date(milliseconds);
    let dd = date.getDate();
    let mm = date.getMonth()+1;
    let yyyy = date.getFullYear();

    if(dd<10) dd = '0'+dd
    if (mm < 10) mm = '0' + mm

    const dateInString = yyyy + '-' + mm + '-' + dd;
    return dateInString
  } else {
    return milliseconds;
  }
}