import axios from 'axios'
import { isObject } from './common'

const api = {
  user: {
    getUserData: {
      path: './dummy-data/users.json'
    }
  },
  task: {
    getAllTask: {
      path: './dummy-data/tasks.json'
    },
    updateTaskData: {
      path: './dummy-data/tasks.json'
    },
  },
  notes: {
    getAllNotes: {
      path: './dummy-data/notes.json'
    },
    updateNote: {
      path: './dummy-data/notes.json'
    }
  },
  category: {
    getAllCategories: {
      path: './dummy-data/categories.json'
    },
    updateCategory: {
      path: './dummy-data/categories.json'
    }
  },
  tags: {
    getAllTags: {
      path: './dummy-data/tags.json'
    },
    updateTag: {
      path: './dummy-data/tags.json'
    }
  }
}

function assignMerger(listToAssign) {
  let listKeys = Object.keys(listToAssign);
  listKeys.forEach((k) => {
    if (k === 'path' || k === 'PATH' || k === 'Path') {
      listToAssign['getUrl'] = function () {
        return this.path;
      };
    } else {
      if (listToAssign[k] && isObject(listToAssign[k]))
        assignMerger(listToAssign[k]);
    }
  });
}
assignMerger(api)

export const callAxios = (options) => {
  return axios(options)
    .then(response => response.data)
    .catch(err => Promise.reject(err))
}

export default api