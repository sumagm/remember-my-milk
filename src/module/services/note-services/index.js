import api, { callAxios } from '../common-services/apilist'
import { isObjectEmpty } from '../common-services/common'

let notes = {}

export default class NoteServices {
  static async getNotes() {
    let url = (api.notes.getAllNotes).getUrl()
    if(isObjectEmpty(notes))
      notes = await callAxios({ method: 'get', url })
    return notes.notes
  }

  static async getNotesOfParticularTask(taskId) {
    let notes = await this.getNotes()
    notes = notes.filter(note => taskId === note.taskId)
    return notes
  }

  static async addNoteData(data) {
    await this.getNotes()
    let lengthBeforeUpdate = notes.notes.length
    return new Promise((res, rej) => {
      if (notes.notes.unshift(data)>lengthBeforeUpdate) res(notes.notes)
      rej({status: 401, message: 'Failed to add the note'})
    })
  }

  static async updateNoteData(data) {
    let flag = 0
    notes.notes.forEach((note, index) => {
      if (note.noteId === data.noteId) {
        notes.notes[index] = { ...note, ...data }
        flag = 1
      }
    })
    return new Promise((res, rej) => {
      flag ? res(notes.notes) : rej({  message: "note not found with given id"  })
    })
  }

  static async deleteNoteData(noteId) {
    await this.getNotes()
    notes.notes = notes.notes.filter(note => note.noteId !== noteId)
    return new Promise(res => {
      res(notes.notes)
    })
  }
}