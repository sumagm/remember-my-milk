import api, { callAxios } from '../common-services/apilist'
import { isObjectEmpty } from '../common-services/common'

let categories = {}

export default class CategoryServices {
  static async getCategories() {
    let url = (api.category.getAllCategories).getUrl()
    if(isObjectEmpty(categories))
      categories = await callAxios({ method: 'get', url })
    return categories.categories
  }

  static async getCategoryById(categoryId) {
    let categories = await this.getCategories()
    categories = categories.filter(category => categoryId === category.categoryId)
    return categories[0]
  }

  static async addCategoryData(data) {
    await this.getCategories()
    let lengthBeforeUpdate = categories.categories.length
    return new Promise((res, rej) => {
      if (categories.categories.unshift(data)>lengthBeforeUpdate) res({ status: 200 })
      rej({status: 401, message: 'Failed to add the category'})
    })
  }

  static async updateCategoryData(data) {
    let flag = 0
    categories.categories.forEach((category, index) => {
      if (category.categoryId === data.categoryId) {
        categories[index] = { ...category, ...data }
        flag = 1
      }
    })
    return new Promise((res, rej) => {
      flag ? res({ status: 200 }) : rej({  message: "category not found with given id"  })
    })
  }

  static async deleteCategoryData(categoryId) {
    await this.getCategories()
    categories.categories = categories.categories.filter(category => category.categoryId !== categoryId)
    return new Promise(res => {
      res({status: 200})
    })
  }
}