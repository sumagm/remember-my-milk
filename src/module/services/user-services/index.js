import api, { callAxios } from '../common-services/apilist'
import { isObjectEmpty } from '../common-services/common'

let userList = {}

export default class UserServices {
  static async getUserList() {
    let url = (api.user.getUserData).getUrl()
    if(isObjectEmpty(userList))
      userList = await callAxios({ method: 'get', url })
    return userList.customerList
  }

  static async getCurrentUserData(userId) {
    let userList = await this.getUserList()
    userList = userList.filter(user => user.userId === userId)
    return userList
  }
}