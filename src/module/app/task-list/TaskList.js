import React, { useState, useEffect } from 'react'
import './style/task-list-style.scss'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faCalendarPlus, faTrash, faUndo, faTimes } from '@fortawesome/free-solid-svg-icons'
import TaskServices from '../../services/task-services'
import { ROUTES } from '../../services/common-services/routes'
import { TIME_IN_MILLISECONDS } from '../../services/common-services/common'
import { Tooltip } from '@material-ui/core'
import TagServices from '../../services/tag-services'
import * as TaskListServices from './service'
import { AlertContext } from '../../services/common-services/common'

let getTaskDetails = {
  userId: TaskListServices.CUR_USER_ID,
  tagId: 0,
  categoryId: 0
};

let selectOptionClasses ={
  incomplete: `${TaskListServices.CSS_CLASS_NAMES.option} ${TaskListServices.CSS_CLASS_NAMES.active}`,
  completed: `${TaskListServices.CSS_CLASS_NAMES.option}`
}

let taskListData = {}

export default function TaskList(props) {
  const [ taskText, setTaskText] = useState('')
  const [ tasksData, setTasksData ] = useState([])
  const [ selectedTaskIds, setSelectedTaskIds ] = useState([])
  const [ postponeListVisibility, setPostponeListVisibility ] = useState(false)
  const [ allSelected, setAllSelected ] = useState(false)
  const [ searchTaskStatus, setSearchTaskStatus ] = useState("Incomplete")
  const [ taskTagDetails, setTaskTagDetails ] = useState({})
  const context = React.useContext(AlertContext)

  useEffect(()=>{
    getTaskData()
  },[taskText,selectedTaskIds,props.location.pathname,props.location.search,searchTaskStatus])

  const getTaskData = () => {
    const propsSearch = new URLSearchParams(props.location.search)
    const todayDate = new Date().getTime();
    switch (props.location.pathname) {
      case ROUTES.ALL_TASKS: getTaskDetails = {...getTaskDetails, categoryId: 0, tagId: 0, taskStatus: searchTaskStatus}
        TaskServices.getCurrentUserTaskData(getTaskDetails).then(data => {
          setTasksData(data)
          
        }).catch(err => console.log(err))
        break
      case ROUTES.TODAY_TASK: getTaskDetails = {...getTaskDetails, taskStatus: searchTaskStatus}
        TaskServices.getTaskByDate(todayDate,(todayDate-TIME_IN_MILLISECONDS.oneDay),getTaskDetails).then(data => setTasksData(data)).catch(err => console.log(err))
        break
      case ROUTES.TOMORROW_TASK: getTaskDetails = {...getTaskDetails, taskStatus: searchTaskStatus}
        TaskServices.getTaskByDate(TIME_IN_MILLISECONDS.nextDay,todayDate,getTaskDetails).then(data => setTasksData(data)).catch(err => console.log(err))
        break
      case ROUTES.THIS_WEEK_TASK: getTaskDetails = {...getTaskDetails, taskStatus: searchTaskStatus}
        TaskServices.getTaskByDate(todayDate,(todayDate-TIME_IN_MILLISECONDS.oneWeek),getTaskDetails).then(data => setTasksData(data)).catch(err => console.log(err))
        break
      case ROUTES.CATEGORY:
        getTaskDetails = {...getTaskDetails, categoryId: propsSearch?propsSearch.get('category'):0, tagId: 0, taskStatus: searchTaskStatus}
        TaskServices.getSpecifiedCategoryTaskData(getTaskDetails).then(data => setTasksData(data)).catch(err => console.log(err))
        break
      case ROUTES.TAG: 
        getTaskDetails = {...getTaskDetails, categoryId: 0, tagId: propsSearch?propsSearch.get('tag'):0, taskStatus: searchTaskStatus}
        TaskServices.getSpecifiedTagTaskData(getTaskDetails).then(data => setTasksData(data)).catch(err => console.log(err))
        break
      default : getTaskDetails = {...getTaskDetails, categoryId: 0, tagId: 0, taskStatus: searchTaskStatus}
        TaskServices.getCurrentUserTaskData(getTaskDetails).then(data => setTasksData(data)).catch(err => console.log(err))
    }
    setTagNamesForTasks()
  }

  const setTagNamesForTasks = () => {
    if(tasksData.length>0) {
      tasksData.map(task => task.taskTags.map(tag => {
        if(!taskListData[task.taskId]) taskListData[task.taskId] = {tagNames:[],tagIds:[]}
        TagServices.getTagsById(tag).then(data => {
          if(!taskListData[task.taskId].tagIds.includes(data.tagId)) {
            taskListData[task.taskId].tagNames.push(data.tagName)
            taskListData[task.taskId].tagIds.push(data.tagId)
          }
          setTaskTagDetails(taskListData)
      }).catch(err => console.log(err))
    }
    ))}
  }

  const isTaskInputValid = () => {
    return taskText===""
  }

  const postponeClickHandler = () => {
    setPostponeListVisibility(!postponeListVisibility)
  }

  const addTask = () => {
    const propsSearch = new URLSearchParams(props.location.search)
    let curSelectedCategory = propsSearch?propsSearch.get('category'):0
    const newTaskData = {
      taskId: Math.random().toString(),
      status: "Incomplete",
      taskText: taskText,
      addedOn: new Date().getTime(),
      dueOn: "",
      taskCategory: curSelectedCategory,
      taskTags: [],
      userId: 1
    }
    TaskServices.addTaskData(newTaskData).then(data => {
      getTaskData()
      setTaskText("")
      context.setAlert('Added Successfully')
    }).catch(err => console.log(err))
  }

  const markAsCompleted = () => {
    if(selectedTaskIds.length>0) {
      const updatedTaskData = { status: "Completed"}
      updateTaskData(updatedTaskData)
    }
  }

  const markAsIncomplete = () => {
    if(selectedTaskIds.length>0) {
      const updatedTaskData = { status: "Incomplete"}
      updateTaskData(updatedTaskData)
    }
  }

  const updateTaskData = (updatedTaskData) => {
    TaskServices.updateTaskData(updatedTaskData,selectedTaskIds).then(data => {
      getTaskData()
      context.setAlert('Updated Successfully')
    }).catch(err => console.log(err))
  }

  const deleteTask = () => {
    if(selectedTaskIds.length>0) {
      TaskServices.deleteTaskData(selectedTaskIds).then(data => {
        getTaskData()
        let taskIds = selectedTaskIds
        taskIds.length = 0
        setSelectedTaskIds(taskIds)
        context.setAlert('Deleted Successfully')
      }).catch(err => console.log(err))
    }
  }

  const onChangeTaskTextHandler = (event) => {
    setTaskText(event.target.value)
  }

  const onCheckHandler = (taskId) => {
    let taskIds = selectedTaskIds
    let item = document.getElementById(`task-check-${taskId}`)
    if(item.type === 'checkbox' && item.checked) {
      if(!taskIds.includes(taskId)) {
        taskIds.push(taskId)
        props.history.push({state: {taskId}})
      }
    } else {
      taskIds = taskIds.filter(task => task!==taskId)
      props.history.push({state: {taskId:0}})
    }
    setSelectedTaskIds(taskIds)
  }

  const allSelectHandler = () => {
    let taskIds = []
    let items = document.getElementsByName('task-check')
    for (let i = 0; i < items.length; i++) {
      if (items[i].type === 'checkbox') {
        if(allSelected) {
          items[i].checked = false
        }
        else {
          items[i].checked = true
          taskIds.push(items[i].value)
        }
        setSelectedTaskIds(taskIds)
      }
    }
    setAllSelected(!allSelected)
  }

  const removeTagForSpecifiedTask = (taskId,tagId) => {
    TaskServices.removeTagOfTask(taskId,tagId).then(data => {
      getTaskData()
      context.setAlert('Removed Successfully')
    }).catch(err => console.log(err))
  }

  const getTagNamesForSpecificTask = (taskId) => {
    return taskTagDetails[taskId].tagNames.map((tag,index) => <div className="task-tag-field" key={index}>
      {tag}
      <span className="task-tag-remove-icon"><FontAwesomeIcon icon={faTimes} onClick={()=> removeTagForSpecifiedTask(taskId,taskTagDetails[taskId].tagIds[index])}/></span>
    </div>)
  }

  const getTaskList = () => {
    return tasksData.map(task => <div key={task.taskId}>
      <div className="task-lists-view"  style={{paddingLeft:"10px"}}>
        <input type="checkbox" name="task-check" id={`task-check-${task.taskId}`} value={task.taskId} onChange={() => onCheckHandler(task.taskId)} />
        {task.taskText}
        <div className="task-tag-names-list">
          {(taskTagDetails[task.taskId] && taskTagDetails[task.taskId].tagNames.length>0) && getTagNamesForSpecificTask(task.taskId)}
        </div>
      </div>
      <hr />
    </div>)
  }

  const postponeSelectHandler = (event) => {
    if(selectedTaskIds.length>0) {
      let postponeTo, updatedTaskData;
      TaskServices.getTaskData(selectedTaskIds[selectedTaskIds.length-1]).then(data => {
        updatedTaskData = {...data}
        postponeTo = data.dueOn
        switch(event.target.value) {
          case TaskListServices.POSTPONE_TASK_DATA.oneDay.value: postponeTo = postponeTo + TIME_IN_MILLISECONDS.oneDay
            break
          case TaskListServices.POSTPONE_TASK_DATA.twoDays.value: postponeTo = postponeTo + TIME_IN_MILLISECONDS.twoDays
            break
          case TaskListServices.POSTPONE_TASK_DATA.oneWeek.value: postponeTo = postponeTo + TIME_IN_MILLISECONDS.oneWeek
            break
          default: postponeTo = postponeTo + TIME_IN_MILLISECONDS.thirtyDays
            break
        }
        updatedTaskData = {...updatedTaskData, dueOn: postponeTo}
        updateTaskData(updatedTaskData)
      }).catch(err => console.log(err))
    }
    postponeClickHandler()
  }

  const updateSearchTaskStatus = (status) => {
    if(status==="Incomplete") {
      setSearchTaskStatus("Incomplete")
      selectOptionClasses.incomplete = `${TaskListServices.CSS_CLASS_NAMES.option} ${TaskListServices.CSS_CLASS_NAMES.active}`
      selectOptionClasses.completed = `${TaskListServices.CSS_CLASS_NAMES.option}`
    } else if (status==="Completed") {
      setSearchTaskStatus("Completed")
      selectOptionClasses.completed = `${TaskListServices.CSS_CLASS_NAMES.option} ${TaskListServices.CSS_CLASS_NAMES.active}`
      selectOptionClasses.incomplete = `${TaskListServices.CSS_CLASS_NAMES.option}`
    }
  }

  return (
    <div className="task-list-container">
      <div className="task-list-field task-list-options">
        <button className={selectOptionClasses.incomplete} onClick={() => {updateSearchTaskStatus("Incomplete")}}>Incomplete</button>
        <button className={selectOptionClasses.completed} onClick={() => {updateSearchTaskStatus("Completed")}}>Completed</button>
      </div>
      <hr />

      <div className="task-list-field task-list-edit-options">
        <Tooltip title='Select All' arrow enterDelay={500}>
          <div className="task-select-list task-icons">
            <input className="fa-icon" type="checkbox" onChange={() => allSelectHandler()}/>
          </div>
        </Tooltip>
        <div className="task-edit-icons">
          {searchTaskStatus !== "Completed" ? (<>
            <Tooltip title='Mark as completed' arrow enterDelay={500}>
              <span className="edit-icon fa-icon task-icons" onClick={() => markAsCompleted()}><FontAwesomeIcon icon={faCheck} /></span>
            </Tooltip>
            <div className="postpone-data-container">
              <Tooltip title='Postpone' arrow enterDelay={500}>
                <span className="edit-icon fa-icon task-icons" onClick={() => postponeClickHandler()}><FontAwesomeIcon icon={faCalendarPlus} /></span>
              </Tooltip>
              {postponeListVisibility && <div className="postpone-list-container">
                <select className="postpone-dropdown-list" onChange={(event) => postponeSelectHandler(event)}>
                  <option label={TaskListServices.POSTPONE_TASK_DATA.oneDay.label} value={TaskListServices.POSTPONE_TASK_DATA.oneDay.value} className="postpone-list"/>
                  <option label={TaskListServices.POSTPONE_TASK_DATA.twoDays.label} value={TaskListServices.POSTPONE_TASK_DATA.twoDays.value} className="postpone-list"/>
                  <option label={TaskListServices.POSTPONE_TASK_DATA.oneWeek.label} value={TaskListServices.POSTPONE_TASK_DATA.oneWeek.value} className="postpone-list"/>
                  <option label={TaskListServices.POSTPONE_TASK_DATA.oneMonth.label} value={TaskListServices.POSTPONE_TASK_DATA.oneMonth.value} className="postpone-list"/>
                </select>
              </div>}
            </div>
            <Tooltip title='Delete' arrow enterDelay={500}>
              <span className="edit-icon fa-icon task-icons" onClick={() => deleteTask()}><FontAwesomeIcon icon={faTrash} /></span>
            </Tooltip>
          </>):<>
            <Tooltip title='Undo to incomplete' arrow enterDelay={500}>
              <span className="edit-icon fa-icon task-icons" onClick={() => markAsIncomplete()}><FontAwesomeIcon icon={faUndo} /></span>
            </Tooltip>
            <Tooltip title='Delete' arrow enterDelay={500}>
              <span className="edit-icon fa-icon task-icons" onClick={() => deleteTask()}><FontAwesomeIcon icon={faTrash} /></span>
            </Tooltip>
          </>}
        </div>
      </div>
      <hr />

      <div className="task-list-field add-task-container">
        <input className="add-task-input-field" type="text" value={taskText} onChange={(event) => onChangeTaskTextHandler(event)} placeholder="Add a task.." />
        <button className="add-task-btn" disabled={isTaskInputValid()} onClick={() => addTask()}>Add Task</button>
      </div>
      <hr />

      {tasksData.length>0 ? getTaskList(): <><div className="task-lists-view" style={{justifyContent:"center"}}>No tasks found</div><hr /></>}
      <div className="task-lists-view"></div><hr />
    </div>
  )
}