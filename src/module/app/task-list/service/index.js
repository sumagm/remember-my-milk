export const CUR_USER_ID = 1;

export const CSS_CLASS_NAMES = {
  option: "option-btn",
  active: "active-btn"
}

export const POSTPONE_TASK_DATA = {
  oneDay: {
    label: "1 Day",
    value: "one-day"
  },
  twoDays: {
    label: "2 Days",
    value: "two-days"
  },
  oneWeek: {
    label: "1 Week",
    value: "one-week"
  },
  oneMonth: {
    label: "1 Month",
    value: "one-month"
  }
}