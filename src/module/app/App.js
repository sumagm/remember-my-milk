import React, { useState, useEffect } from 'react'
import Navigation from './navigation/Navigation'
import TaskList from './task-list/TaskList'
import TaskDetails from './task-details/TaskDetails'
import '../../styles/style.scss'
import { ROUTES } from '../services/common-services/routes'
import Alert from './ui-components/alert/Alert'
import { AlertContext } from '../services/common-services/common'
import { withRouter } from 'react-router-dom'
import registerInterceptors from '../services/common-services/interceptors'
import {
  Redirect,
  Route,
  Switch,
} from 'react-router-dom'

function App(props) {
  const [ alertVisibilityState, setAlertShowState ] = useState(false);
  const [ alertMsg, setAlertMsg ] = useState('')

  useEffect(()=>{
    registerInterceptors(props.history)
  },[])

  const toggleAlert = () => {
    setAlertShowState(!alertVisibilityState)
  }

  const setAlert = (msg) => {
    setAlertMsg(msg)
    toggleAlert()
  }

  return (
    <AlertContext.Provider value={{alertVisibilityState, alertMsg, setAlert, toggleAlert}}>
      <div className="app">
        <div className="navigation-view"><Navigation /></div>
        <div className="task-list-view full-width">
          <Switch>
            <Route
              path={ROUTES.BASE}
              render={props => <TaskList {...props} />}
            />
            <Route
              path={ROUTES.ALL_TASKS}
              render={props => <TaskList {...props} />}
            />
            <Route
              path={ROUTES.TAG}
              render={props => <TaskList {...props} />}
            />
            <Route
              path={ROUTES.TAG+'/:tag'}
              render={props => <TaskList {...props} />}
            />
            <Route
              path={ROUTES.CATEGORY}
              render={props => <TaskList {...props} />}
            />
            <Route
              path={ROUTES.CATEGORY+'/:category'}
              render={props => <TaskList {...props} />}
            />
            <Redirect to={ROUTES.BASE} />
          </Switch>
        </div>
        <div className="task-detail-view full-width"><TaskDetails /></div>
        <Alert />
      </div>
    </AlertContext.Provider>
  );
}

export default withRouter(App)
