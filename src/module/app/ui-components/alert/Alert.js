import React from 'react'
import SweetAlert from 'sweetalert-react'
import '../../../../../node_modules/sweetalert/dist/sweetalert.css'
import { AlertContext } from '../../../services/common-services/common'

const Alert = (props) => {
  const context = React.useContext(AlertContext)

  return (
    <div>
      <SweetAlert
        show={context.alertVisibilityState}
        title=""
        text={context.alertMsg}
        onConfirm={() => context.toggleAlert()}
      />
    </div>
  );
}

export default Alert;