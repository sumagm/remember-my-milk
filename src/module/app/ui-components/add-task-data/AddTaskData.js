import React, { useState } from 'react'
import './style/add-task-data.scss'

export default function AddTaskData(props) {
  const [ taskData, setTaskData ] = useState('')

  const onChangeHandler = (event) => {
    setTaskData(event.target.value)
  }

  const isTaskDataValid = () => {
    return taskData===""
  }

  return (
    <div className="add-task-data-container">
      <div className="add-task-data-view">
        <label className="add-task-data-label">{props.label}</label>
        <div className="task-data-input-field">
          <span className="task-data-subtitle">{props.subtitle}</span>
          <input className="task-add-data-field" type="text" value={taskData} onChange={(event) => onChangeHandler(event)}/>
        </div>
        <div className="add-task-data-handler-btn">
          <button className="add-task-data-btn add-btn" disabled={isTaskDataValid()} onClick={() => props.addBtnHandler(taskData)}>Add</button>
          <button className="add-task-data-btn cancel-btn" onClick={props.cancelBtnHandler}>Cancel</button>
        </div>
      </div>
    </div>
  )
}
