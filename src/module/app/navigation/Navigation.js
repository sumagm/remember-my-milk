import React, { useState, useEffect } from 'react'
import './style/navigation-style.scss'
import CategoryServices from '../../services/category-services'
import TagServices from '../../services/tag-services'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretRight, faPlusSquare } from '@fortawesome/free-solid-svg-icons'
import AddTaskData from '../ui-components/add-task-data/AddTaskData'
import { ROUTES } from '../../services/common-services/routes'
import { withRouter } from 'react-router-dom'
import { Link } from 'react-router-dom'
import * as NavigationServices from './service'
import { AlertContext } from '../../services/common-services/common'

function Navigation(props) {
  const [ tags, setTags ] = useState({})
  const [ categories, setCategories ] = useState({})
  const [ inboxListItems, setInboxListItems ] = useState(false)
  const [ categoryListItems, setCategoryListItems ] = useState(false)
  const [ tagsListItems, setTagsListItems ] = useState(false)
  const [ isAddTagVisible, setIsAddTagVisible ] = useState(false)
  const [ isAddCategoryVisible, setIsAddCategoryVisible ] = useState(false)
  const context = React.useContext(AlertContext)

  useEffect(()=>{
    getUpdatedData()
  }, [tags,categories])

  const getUpdatedData = () => {
    TagServices.getTags().then(data => setTags(data)).catch(err => console.log(err))
    CategoryServices.getCategories().then(data => setCategories(data)).catch(err => console.log(err))
  }

  const changeTagRouterPath = (tagId) => {
    props.history.push({pathname:ROUTES.TAG, search: `?tag=${tagId}` })
  }

  const changeCategoryRouterPath = (categoryId) => {
    props.history.push({pathname:ROUTES.CATEGORY, search: `?category=${categoryId}` })
  }

  const getTagsList = () => {
    return tags.map(tag => <span className="sub-navigation-list-item line-height" onClick={() => changeTagRouterPath(tag.tagId)}>{tag.tagName}</span>)
  }

  const getCategoriesList = () => {
    return categories.map(category => <span className="sub-navigation-list-item line-height" onClick={() => changeCategoryRouterPath(category.categoryId)}>{category.categoryName}</span>)
  }

  const setAddTagViewVisibility = () => {
    setIsAddTagVisible(!isAddTagVisible)
  }

  const setAddCategoryViewVisibility = () => {
    setIsAddCategoryVisible(!isAddCategoryVisible)
  }

  const addTag = (tagData) => {
    const addTagData = {
      tagId: Math.random().toString(),
      tagName: tagData
    };
    TagServices.addTagData(addTagData).then(data => {
      setTags(data)
      context.setAlert('Added Successfully')
    }).catch(err => console.log(err))
    setAddTagViewVisibility()
  }

  const addCategory = (categoryData) => {
    const addCategoryData = {
      categoryId: Math.random().toString(),
      categoryName: categoryData
    };
    CategoryServices.addCategoryData(addCategoryData).then(data => {
      setCategories(data)
      context.setAlert('Added Successfully')
    }).catch(err => console.log(err))
    setAddCategoryViewVisibility()
  }

  const setListItemsViewVisibility = (type) => {
    switch(type) {
      case NavigationServices.LIST_ITEMS_TYPE.INBOX : setInboxListItems(!inboxListItems)
          break;
      case NavigationServices.LIST_ITEMS_TYPE.TAGS: setTagsListItems(!tagsListItems)
          break;
      case NavigationServices.LIST_ITEMS_TYPE.CATEGORY: setCategoryListItems(!categoryListItems)
          break;
      default: break
    }
  }

  return (
    <div className="navigation-container">
      <div className="navigation-heading"><span>Remember the milk</span></div>
      <div className="navigation-lists">
        <div className="navigation-list-item">
          <div className="list-item-view line-height">
            <span className="navigate-item-icon fa-icon" onClick={() => setListItemsViewVisibility(NavigationServices.LIST_ITEMS_TYPE.INBOX)}><FontAwesomeIcon icon={faCaretRight} /></span>
            <span className="navigate-item-header">Inbox</span>
          </div>
          {inboxListItems && <div className="list-item-view sub-navigation-list">
            <ul className="nav">
              <li className="nav-list">
                <Link className="nav-list-link" to={ROUTES.ALL_TASKS}>
                  <span className="sub-navigation-list-item line-height">All Tasks</span>
                </Link>
              </li>
              <li className="nav-list">
                <Link className="nav-list-link" to={ROUTES.TODAY_TASK}>
                  <span className="sub-navigation-list-item line-height">Today</span>
                </Link>
              </li>
              <li className="nav-list">
                <Link className="nav-list-link" to={ROUTES.TOMORROW_TASK}>
                  <span className="sub-navigation-list-item line-height">Tomorrow</span>
                </Link>
              </li>
              <li className="nav-list">
                <Link className="nav-list-link" to={ROUTES.THIS_WEEK_TASK}>
                  <span className="sub-navigation-list-item line-height">This Week</span>
                </Link>
              </li>
            </ul>
          </div>}
        </div>
        <hr />
        <div className="navigation-list-item">
          <div className="list-item-view line-height">
            <div className="navigate-item-icon fa-icon" onClick={() => setListItemsViewVisibility(NavigationServices.LIST_ITEMS_TYPE.CATEGORY)}><FontAwesomeIcon icon={faCaretRight} /></div>
            <div className="navigate-item-header list-item-header"><span>Category</span><span className="fa-icon" onClick={setAddCategoryViewVisibility}><FontAwesomeIcon icon={faPlusSquare} /></span></div>
          </div>
          {(categories.length>0 && categoryListItems) && <div className="list-item-view sub-navigation-list">
            {getCategoriesList()}
          </div>}
        </div>
        <hr />
        <div className="navigation-list-item">
          <div className="list-item-view line-height">
            <div className="navigate-item-icon fa-icon" onClick={() => setListItemsViewVisibility(NavigationServices.LIST_ITEMS_TYPE.TAGS)}><FontAwesomeIcon icon={faCaretRight} /></div>
            <div className="navigate-item-header list-item-header"><span>Tags</span><span className="fa-icon" onClick={setAddTagViewVisibility}><FontAwesomeIcon icon={faPlusSquare} /></span></div>
          </div>
          {(tags.length>0 && tagsListItems) && <div className="list-item-view sub-navigation-list">
            {getTagsList()}
          </div>}
        </div>
        <hr />
      </div>
      {isAddCategoryVisible && <AddTaskData subtitle="Please enter a new category" label={NavigationServices.ADD_TASK_DATA_LABELS.CATEGORY} cancelBtnHandler={setAddCategoryViewVisibility} addBtnHandler={addCategory}/>}
      {isAddTagVisible && <AddTaskData subtitle="Please enter a new tag" label={NavigationServices.ADD_TASK_DATA_LABELS.TAG} cancelBtnHandler={setAddTagViewVisibility} addBtnHandler={addTag}/>}
    </div>
  )
}

export default withRouter(Navigation)