export const LIST_ITEMS_TYPE = {
  INBOX: 'INBOX',
  TAGS: 'TAGS',
  CATEGORY: 'CATEGORY'
};

export const ADD_TASK_DATA_LABELS = {
  TAG: "Add a new tag",
  CATEGORY: "Add a new category"
}