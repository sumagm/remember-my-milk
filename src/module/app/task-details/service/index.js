import CategoryServices from '../../../services/category-services'
import TagServices from '../../../services/tag-services'
import AddTaskData from '../../ui-components/add-task-data/AddTaskData'
import NoteServices from '../../../services/note-services'
import TaskServices from '../../../services/task-services'

export const ADD_FIELD_TYPE = {
  TAG: "Tag",
  CATEGORY: "Category"
}