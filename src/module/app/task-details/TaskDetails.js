import React, { useState, useEffect } from 'react'
import { withRouter } from 'react-router-dom'
import './style/task-detail-style.scss'
import TaskServices from '../../services/task-services'
import { getDateForInputValue } from '../../services/common-services/common'
import CategoryServices from '../../services/category-services'
import TagServices from '../../services/tag-services'
import AddTaskData from '../ui-components/add-task-data/AddTaskData'
import NoteServices from '../../services/note-services'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrash, faEdit, faTimes } from '@fortawesome/free-solid-svg-icons'
import * as TaskDetailServices from './service'
import { AlertContext } from '../../services/common-services/common'

let taskTagNames = []
let taskTagIds=[]

function TaskDetails(props) {
  const [taskText,setTaskText] = useState('')
  const [taskDueData,setTaskDueData] = useState('')
  const [taskCategory,setTaskCategory] = useState('')
  const [taskTag,setTaskTag] = useState([])
  const [taskNotes,setTaskNotes] = useState([])
  const [taskNote, setTaskNote ] = useState('')
  const [categoryName, setCategoryName] = useState('')
  const [addTagVisible, setAddTagVisible] = useState(false)
  const [addCategoryVisible, setAddCategoryVisible] = useState(false)
  const [taskId, setTaskId] = useState(0)
  const [selectedNoteId, setSelectedNoteId] = useState(0)
  const [editTaskNote, setEditTaskNote] = useState('')
  const context = React.useContext(AlertContext)

  useEffect(() => {
    getTaskData()
  },[props.history.location.state, taskId, taskCategory, taskTag])

  const getTaskData = () => {
    let propsState = props.history.location.state
    setTaskId(propsState&&propsState.taskId)
    TaskServices.getTaskData(taskId).then(data => {
      if(Object.keys(data).length>0) {
        setTaskText(data.taskText)
        setTaskCategory(data.taskCategory)
        setTaskDueData(data.dueOn)
        setTaskTag(data.taskTags)
        CategoryServices.getCategoryById(taskCategory).then(data => setCategoryName(data.categoryName)).catch(err => console.log(err))
        taskTag.map(tag => {
          taskTagNames = []
          taskTagIds = []
          TagServices.getTagsById(tag).then(data => addTagDetails(data.tagName, data.tagId)).catch(err => console.log(err))
        })
        NoteServices.getNotesOfParticularTask(taskId).then(data => setTaskNotes(data)).catch(err => console.log(err))
      }
    }).catch(err => console.log(err))
  }

  const addTagDetails = (name, id) => {
    if(!taskTagIds.includes(id)) {
      taskTagNames.push(name)
      taskTagIds.push(id)
    }
  }

  const removeTagForSpecifiedTask = (tagId) => {
    if(taskId!==0) {
      TaskServices.removeTagOfTask(taskId,tagId).then(data => {
        getTaskData()
        context.setAlert('Removed Successfully')
      }).catch(err => console.log(err))
    }
  }

  const getTaskTagNames = () => {
    return taskTagNames.map((tag,index) => <div className="task-data-input-field task-tag-field" key={index}>
      {tag}
      <span className="task-tag-remove-icon"><FontAwesomeIcon icon={faTimes} onClick={()=> removeTagForSpecifiedTask(taskTagIds[index])}/></span>
    </div>)
  }

  const taskNoteEditClickHandler = (noteId,noteText) => {
    setEditTaskNote(noteText)
    setSelectedNoteId(noteId)
  }

  const deleteNoteClickHandler = (noteId) => {
    NoteServices.deleteNoteData(noteId).then(data => {
      setTaskNotes(data)
      context.setAlert('Deleted Successfully')
    }).catch(err => console.log(err))
  }

  const editNote = () => {
    NoteServices.updateNoteData({noteId:selectedNoteId,noteText:editTaskNote}).then(data => {
      setTaskNotes(data)
      context.setAlert('Updated Successfully')
    }).catch(err=>console.log(err))
    setSelectedNoteId(0)
  }

  const getTaskNotesList = () => {
    return taskNotes.map(note => <div key={note.noteId}>
      {selectedNoteId!==note.noteId?<div className="added-task-note">
        <div>{note.noteText}</div>
        <div>
          <FontAwesomeIcon className="note-icon" icon={faEdit} onClick={() => taskNoteEditClickHandler(note.noteId, note.noteText)}/>
          <FontAwesomeIcon className="note-icon" icon={faTrash} onClick={() => deleteNoteClickHandler(note.noteId)}/></div>
      </div>:<div className="note-edit-field">
        <input type="text" value={editTaskNote} className="note-edit-input-field" onChange={(event) => setInputTaskData(event,"editNote")}/>
        <button className="note-edit-input-btn" onClick={() => editNote()}>Edit</button>
      </div>}
      <hr />
    </div>)
  }

  const setCategorySelectHandler = () => {
    setAddCategoryVisible(!addCategoryVisible)
  }

  const setTagSelectHandler = () => {
    setAddTagVisible(!addTagVisible)
  }

  const addCategory = (receivedCategory) => {
    if(taskCategory===0 || !taskCategory) {
      const newCatId = Math.random().toString();
      const newCategoryData = {
        categoryId: newCatId,
        categoryName: receivedCategory
      }
      CategoryServices.addCategoryData(newCategoryData).then(data => {
        setTaskCategory(newCatId)
        let updatedTaskData = {taskCategory: newCatId}
        updateTaskData(updatedTaskData)
      }).catch(err => console.log(err))
    } else context.setAlert('This task is already have a category')
    setCategorySelectHandler()
  }

  const addTag = (receivedTag) => {
      const newTagId = Math.random().toString();
      const newTagData = {
        tagId: newTagId,
        tagName: receivedTag
      }
      TagServices.addTagData(newTagData).then(data => {
        setTaskCategory(newTagId)
        let curTaskTags = taskTag
        curTaskTags.push(newTagId)
        let updatedTaskData = {taskTag: curTaskTags}
        updateTaskData(updatedTaskData)
      }).catch(err => console.log(err))
    setTagSelectHandler()
  }

  const updateTaskData = (updatedTaskData) => {
    TaskServices.updateTaskData(updatedTaskData,[taskId]).then(data => {
      context.setAlert('Updated Successfully')
    }).catch(err => console.log(err))
  }

  const onSelectAddTaskDataHandler = (event) => {
    switch(event.target.value) {
      case TaskDetailServices.ADD_FIELD_TYPE.CATEGORY : setCategorySelectHandler()
        break
      default: setTagSelectHandler()
    }
  }

  const editTaskHandler = () => {
    const updatedTaskData = {
      taskCategory: taskCategory,
      taskText: taskText,
      dueOn: new Date(taskDueData).getTime(),
      taskTags: taskTag
    }
    updateTaskData(updatedTaskData)
  }

  const setInputTaskData = (event, type) => {
    let value = event.target.value
    switch(type) {
      case "taskText": setTaskText(value)
        break
      case "dueData": setTaskDueData(value)
        break
      case "editNote": setEditTaskNote(value)
        break
      default: break
    }
  }

  const onTaskNoteChangeHandler = (event) => {
    setTaskNote(event.target.value)
  }

  const checkTaskNoteValidation = () => {
    return taskNote!=='' && taskId!==0
  }

  const addTaskNote = () => {
    let newTaskNote = {
      noteId: Math.random().toString(),
      noteText: taskNote,
      noteAddedOn: new Date().getTime(),
      taskId: taskId
    }
    NoteServices.addNoteData(newTaskNote).then(data => {
      setTaskNotes(data)
      setTaskNote('')
      context.setAlert('Added Successfully')
    }).catch(err => console.log(err))
  }

  return (
    <div className="task-detail-container">
      <div className="task-detail-info-view">
        <div className="task-text-data">
          <input className="task-data-input-field task-text-field" type="text" value={taskText} onChange={(event) => setInputTaskData(event,"taskText")}/>
        </div>
        <div className="task-full-info">
          <div className="task-full-info-data due-data">
            <label className="task-data-field-label">Due</label>
            <input className="task-data-input-field due-data-field" type="date" value={getDateForInputValue(taskDueData)} onChange={(event) => setInputTaskData(event,"dueData")}/>
          </div>
          {taskCategory!==0 && <div className="task-full-info-data task-category">
            <label className="task-data-field-label">Category</label>
            <input className="task-data-input-field category-data-field" type="text" disabled value={categoryName}/>
          </div>}
          {(taskTag && taskTag.length>0) && <div className="task-full-info-data task-tag">
            <label className="task-data-field-label">Tag</label>
            {getTaskTagNames()}
          </div>}
          <div className="task-full-info-data add-extra-data">
            <label className="task-data-field-label">Add Field</label>
            <select className="task-data-input-field add-task-field-list" onChange={(event) => onSelectAddTaskDataHandler(event)}>
              <option label={TaskDetailServices.ADD_FIELD_TYPE.TAG} value={TaskDetailServices.ADD_FIELD_TYPE.TAG}/>
              <option label={TaskDetailServices.ADD_FIELD_TYPE.CATEGORY} value={TaskDetailServices.ADD_FIELD_TYPE.CATEGORY}/>
            </select>
          </div>
        </div>
        <div className="edit-task-btn-container"><button className="edit-task-data-btn" onClick={() => editTaskHandler()}>Edit</button></div>
      </div>
      <hr />
      <div className="task-notes-container">
        <div className="add-task-note-container">
          <input className="add-note-field" type="text" name="task" value={taskNote} placeholder="Add note.." onChange={(event) => onTaskNoteChangeHandler(event)}/>
          <button className="add-note-btn" onClick={()=> addTaskNote()} disabled={!checkTaskNoteValidation()}>Add Note</button>
        </div>
        <hr />
        {taskNotes.length>0 && <div className="added-task-notes-container">
          {getTaskNotesList()}
          <div className="added-task-note"></div><hr />  
        </div>}
      </div>
      {addTagVisible && <AddTaskData label="Add a tag" subtitle="Pleaser enter a new tag" addBtnHandler={addTag} cancelBtnHandler={setTagSelectHandler}/>}
      {addCategoryVisible && <AddTaskData label="Add a category" subtitle="Please enter a new category" addBtnHandler={addCategory} cancelBtnHandler={setCategorySelectHandler}/>}
    </div>
  )
}

export default withRouter(TaskDetails)